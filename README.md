### dev Docker env
```bash
docker compose -f docker-compose-local.yml up


# On  Hetzner server 
docker compose logs -f

# Manual start On  Hetzner server
docker compose --env-file .env  up -d
```

### Links
https://www.nginx.com/resources/wiki/start/topics/recipes/drupal/

